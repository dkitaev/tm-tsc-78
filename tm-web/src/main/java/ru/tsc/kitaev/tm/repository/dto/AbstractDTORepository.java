package ru.tsc.kitaev.tm.repository.dto;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.NoRepositoryBean;
import ru.tsc.kitaev.tm.dto.AbstractEntityDTO;

@NoRepositoryBean
public interface AbstractDTORepository<E extends AbstractEntityDTO> extends JpaRepository<E, String> {

}
