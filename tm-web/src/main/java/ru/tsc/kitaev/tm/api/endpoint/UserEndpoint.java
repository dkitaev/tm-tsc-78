package ru.tsc.kitaev.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import ru.tsc.kitaev.tm.dto.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@RequestMapping("/api/users")
public interface UserEndpoint {

    @NotNull
    @WebMethod
    @PostMapping("/add")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    UserDTO add(
            @WebParam(name = "user", partName = "user")
            @NotNull @RequestBody final UserDTO user
    );

    @NotNull
    @WebMethod
    @PutMapping("/save")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    UserDTO save(
            @WebParam(name = "user", partName = "user")
            @NotNull @RequestBody final UserDTO user
    );

    @Nullable
    @WebMethod
    @GetMapping("/findAll")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    List<UserDTO> findAll();

    @Nullable
    @WebMethod
    @GetMapping("/findById/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    UserDTO findById(
            @WebParam(name = "id", partName = "id")
            @NotNull @PathVariable(value = "id") final String id
    );

    @WebMethod
    @GetMapping("/existsById/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    boolean existsById(
            @WebParam(name = "id", partName = "id")
            @Nullable @PathVariable(value = "id") final String id
    );

    @WebMethod
    @GetMapping("/count")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    int getSize();

    @WebMethod
    @DeleteMapping("/delete")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    void delete(
            @WebParam(name = "user", partName = "user")
            @NotNull @RequestBody final UserDTO user
    );

    @WebMethod
    @DeleteMapping("/deleteById/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    void deleteById(
            @WebParam(name = "id", partName = "id")
            @NotNull @PathVariable(value = "id") final String id
    );

    @Nullable
    @WebMethod
    @GetMapping("/findByLogin/{login}")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    UserDTO findByLogin(
            @WebParam(name = "login", partName = "login")
            @NotNull @PathVariable(value = "login") final String login
    );

}
