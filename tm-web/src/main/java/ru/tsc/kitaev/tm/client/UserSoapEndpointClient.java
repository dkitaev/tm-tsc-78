package ru.tsc.kitaev.tm.client;

import org.jetbrains.annotations.NotNull;
import ru.tsc.kitaev.tm.api.endpoint.UserEndpoint;

import javax.xml.namespace.QName;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Service;
import java.net.MalformedURLException;
import java.net.URL;

public class UserSoapEndpointClient {

    public static UserEndpoint getInstance(@NotNull final String baseUrl) throws MalformedURLException {
        @NotNull final String wsdl = baseUrl + "/ws/UserEndpoint?wsdl";
        @NotNull final URL url = new URL(wsdl);
        @NotNull final String lp = "UserEndpointImplService";
        @NotNull final String ns = "http://endpoint.tm.kitaev.tsc.ru/";
        @NotNull final QName name = new QName(ns, lp);
        @NotNull final UserEndpoint result = Service.create(url, name).getPort(UserEndpoint.class);
        @NotNull final BindingProvider bindingProvider = (BindingProvider) result;
        bindingProvider.getRequestContext().put(BindingProvider.SESSION_MAINTAIN_PROPERTY, true);
        return result;
    }

}
