package ru.tsc.kitaev.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.lang.Nullable;
import ru.tsc.kitaev.tm.model.AbstractOwnerModel;

import java.util.List;

@NoRepositoryBean
public interface AbstractOwnerRepository<E extends AbstractOwnerModel> extends AbstractRepository<E> {

    @Nullable
    List<E> findAllByUserId(@NotNull String userId);

    @Nullable
    E findByUserIdAndId(@NotNull String userId, @NotNull String id);

    boolean existsByUserIdAndId(@NotNull String userId, @NotNull String id);

    long countByUserId(@NotNull String userId);

    void deleteAllByUserId(@NotNull String userId);

    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

}
