package ru.tsc.kitaev.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.tsc.kitaev.tm.api.service.dto.IProjectDTOService;
import ru.tsc.kitaev.tm.api.service.model.IProjectService;
import ru.tsc.kitaev.tm.dto.CustomUser;
import ru.tsc.kitaev.tm.enumerated.Status;
import ru.tsc.kitaev.tm.dto.ProjectDTO;
import ru.tsc.kitaev.tm.model.Project;

@Controller
public class ProjectController {

    @Autowired
    private IProjectDTOService projectDTOService;

    @Autowired
    private IProjectService projectService;

    @GetMapping("/project/create")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public String create(
            @AuthenticationPrincipal final CustomUser user
    ) {
        projectDTOService.add(user.getUserId(), new ProjectDTO("New Project " + System.currentTimeMillis()));
        return "redirect:/projects";
    }

    @GetMapping("/project/delete/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public String delete(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") String id
    ) {
        projectService.deleteById(user.getUserId(), id);
        return "redirect:/projects";
    }

    @PostMapping("/project/edit/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public String edit(
            @AuthenticationPrincipal final CustomUser user,
            @ModelAttribute("project") ProjectDTO project,
            BindingResult result
    ) {
        project.setUserId(user.getUserId());
        projectDTOService.update(project);
        return "redirect:/projects";
    }

    @GetMapping("/project/edit/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    public ModelAndView edit(
            @AuthenticationPrincipal final CustomUser user,
            @PathVariable("id") String id
    ) {
        final Project project = projectService.findById(user.getUserId(), id);
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("project-edit");
        modelAndView.addObject("project", project);
        modelAndView.addObject("statuses", getStatus());
        return modelAndView;
    }

    public Status[] getStatus() {
        return Status.values();
    }

}
