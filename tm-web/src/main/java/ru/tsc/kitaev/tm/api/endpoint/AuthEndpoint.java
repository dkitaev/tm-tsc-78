package ru.tsc.kitaev.tm.api.endpoint;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.tsc.kitaev.tm.dto.Result;
import ru.tsc.kitaev.tm.dto.UserDTO;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
@RequestMapping("/api/auth")
public interface AuthEndpoint {

    @WebMethod
    @PostMapping("/login")
    Result login(
            @WebParam(name = "username") String username,
            @WebParam(name = "password") String password
    );

    @WebMethod
    @GetMapping("profile")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    UserDTO profile();

    @WebMethod
    @PostMapping("/logout")
    @PreAuthorize("hasRole('ADMINISTRATOR') OR hasRole('USER')")
    Result logout();

}
