package ru.tsc.kitaev.tm.api.service.dto;

import ru.tsc.kitaev.tm.dto.ProjectDTO;

public interface IProjectDTOService extends IOwnerDTOService<ProjectDTO> {

}
