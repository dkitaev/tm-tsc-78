package ru.tsc.kitaev.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.kitaev.tm.model.User;;

public interface IUserService extends IService<User> {

    @Nullable
    User findByLogin(@NotNull String login);

}
